#!../../bin/linux-x86_64/plc

## You may have to change plc to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/plc.dbd"
plc_registerRecordDeviceDriver pdbbase

# Use the following commands for TCP/IP
#drvAsynIPPortConfigure(const char *portName,
#                       const char *hostInfo,
#                       unsigned int priority,
#                       int noAutoConnect,
#                       int noProcessEos);
# This line is for Modbus TCP
#drvAsynIPPortConfigure("plcgas","192.168.10.20:502",0,0,1)
drvAsynIPPortConfigure("plcgas","$(DEVIP):$(DEVPORT)",0,0,1)

# test
#drvAsynIPPortConfigure("plcgas","127.0.0.1:50123",0,0,1)

#modbusInterposeConfig(const char *portName,
#                      modbusLinkType linkType,
#                      int timeoutMsec, 
#                      int writeDelayMsec)
# This line is for Modbus TCP
modbusInterposeConfig("plcgas",0,2000,0)

# Word access at Modbus address 0
# Access 35 words as outputs.  
# Function code=223
# Default data type unsigned integer.
# drvModbusAsynConfigure("portName", "tcpPortName", slaveAddress, modbusFunction, modbusStartAddress, modbusLength, dataType, pollMsec, "plcType")
drvModbusAsynConfigure(  "Out_Word",      "plcgas",            0,            223,                  0,           25,        0,      100, "PhC_AXC_1050")

# Bit access at Modbus address 25
# Access 4 words for bit outputs.
# Function code=6
# drvModbusAsynConfigure("portName", "tcpPortName", slaveAddress, modbusFunction, modbusStartAddress, modbusLength, dataType, pollMsec,      "plcType")
drvModbusAsynConfigure(  "Out_Bits",      "plcgas",            0,              6,                 25,           15,        0,      100, "PhC_AXC_1050")

# Word access at Modbus address 40
# Access 41 words as inputs.  
# Function code=123
# default data type unsigned integer.
# drvModbusAsynConfigure("portName", "tcpPortName", slaveAddress, modbusFunction, modbusStartAddress, modbusLength, dataType, pollMsec, "plcType")
drvModbusAsynConfigure(   "In_Word",      "plcgas",            0,            123,                 40,           80,        0,      500, "PhC_AXC_1050")

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadTemplate("plc_gas.substitutions")
dbLoadRecords("plcgas.db", "BL=PINK, DEV=PLCGAS")

## Autosave Settings
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK, DEV=PLCGAS")

## Start any sequence programs
#seq sncxxx,"user=epics"
